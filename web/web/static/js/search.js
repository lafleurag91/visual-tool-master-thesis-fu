$(function(){

    $.getJSON('/get_user_list', {}, function (user_history_list) {
        var userlist = $("<ul></ul>").addClass("list-group");
        var users = user_history_list["user_id_list"]
        for (var i=0; i< users.length; i++){
            useritem = $("<li></li>").addClass("list-group-item").text(users[i]);
            userlist.append(useritem);
        }
        $("#user_id_list").append(userlist);
    });

    function comp(a,b){ return new Date(a.created_at).getTime() - new Date(b.created_at).getTime(); }

    $("#show_hide_list").on("click", function(e){
         if ($('#filter').is(':hidden')) {
            $('#filter').show('slide',{direction:'left'},1000);
         } else {
            $('#filter').hide('slide',{direction:'left'},1000);
         }
    });

    $("#user_search").on("click", function(e){
        $("#user_history_div").empty();
        $("#mention_graph_div").empty();
        user_input = $("#user_input").val();
        $.getJSON('/search_user_history', {user:user_input}, function (user_history_list) {
            user_history = user_history_list["data"]
            user_history = JSON.parse(user_history);
            user_history.sort(comp);

            var table = '<table id="user_history_table" data-toggle="table" class="table" data-sort-name="created_at" data-sort-order="desc"><thead>' +
                         '<tr><th>User\'s Tweet Identifier</th><th data-field="created_at" data-sortable="true">Created At</th><th>Tweet</th></tr></thead>' +
                         '<tbody>'
            var table_body = $("#user_history_table").find("tbody");
            for (var i=0; i < user_history.length; i++) {
                tweet = user_history[i]["tweet"];
                if (typeof(tweet) == "undefined"){
                    tweet = user_history[i]["text"];
                }
                table += "<tr><td>"+ i + "</td><td>"+ user_history[i]["created_at"] + "</td> <td>"+ tweet + "</td></tr>";
            }
            table += '</tbody></table>'

            $("#user_history_div").append(table);
            load_chart('timeline_div',user_history);
            mention_graph_div = "#mention_graph_div";
            build_graph(mention_graph_div, user_history_list["mention_graph"]);
            display_statistics("#user_statistics",user_history_list["statistics"])

        });

    });

    $("#user_search_tab4").on("click", function(e){
        $("#graph").empty();
        user_input = $("#user_input_tab4").val();
        $.getJSON('/get_graph_structure', {user : user_input}, function (data) {
            if (data["error"]){
                alert(data["error"]);
            }
            else{
                build_graph("#graph", data);
            }
        });
    });


    $("#hashtag_search").on("click", function(e){
        $("#hashtag_history_div").empty();
        hashtag_input = $("#hashtag_input").val();
        $.getJSON('/search_hashtag', {hashtag : hashtag_input}, function (hashtag_list) {
            hashtag_data = hashtag_list["data"];
            hashtag_data = JSON.parse(hashtag_data);
            hashtag_data.sort(comp);

            var table = '<table id="hashtag_table" data-toggle="table" class="table" data-sort-name="created_at" data-sort-order="desc"><thead>' +
                         '<tr><th>User\'s Tweet Identifier</th><th data-field="created_at" data-sortable="true">Created At</th><th>User ID</th><th>Tweet</th></tr></thead>' +
                         '<tbody>'
            var table_body = $("#hashtag_table").find("tbody");
            for (var i=0; i < hashtag_data.length; i++) {
                userid = hashtag_data[i]["user_id"];
                if (typeof(userid) == "undefined"){
                    userid = hashtag_data[i]["userid"]
                }
                tweet = hashtag_data[i]["tweet"]
                if (typeof(tweet) == "undefined"){
                    tweet = hashtag_data[i]["text"]
                }
                table += "<tr><td>"+ i + "</td><td>"+ hashtag_data[i]["created_at"] + "</td> <td>"+ userid + "</td><td>"+ tweet + "</td></tr>";
            }
            table += '</tbody></table>';

            $("#hashtag_history_div").append(table);
            load_chart('hashtag_timeline_div',hashtag_data);
            //mention_graph_div = "#mention_graph_div";
            //build_graph(mention_graph_div, user_history_list["mention_graph"]);

        });
    });

    $("#mention_search").on("click", function(e){
        $("#mention_history_div").empty();
        mention_input = $("#mention_input").val();
        $.getJSON('/search_mention', {mention : mention_input}, function (mention_list) {
            mention_data = mention_list["data"];
            mention_data = JSON.parse(mention_data);
            mention_data.sort(comp);

            var table = '<table id="mention_table" data-toggle="table" class="table" data-sort-name="created_at" data-sort-order="desc"><thead>' +
                         '<tr><th>User\'s Tweet Identifier</th><th data-field="created_at" data-sortable="true">Created At</th><th>User ID</th><th>Tweet</th></tr></thead>' +
                         '<tbody>'
            var table_body = $("#mention_table").find("tbody");
            for (var i=0; i < mention_data.length; i++) {
                 userid = mention_data[i]["user_id"];
                if (typeof(userid) == "undefined"){
                    userid = mention_data[i]["userid"]
                }
                tweet = mention_data[i]["tweet"]
                if (typeof(tweet) == "undefined"){
                    tweet = mention_data[i]["text"]
                }
                table += "<tr><td>"+ i + "</td><td>"+ mention_data[i]["created_at"] + "</td> <td>"+ userid + "</td><td>"+ tweet + "</td></tr>";
            }
            table += '</tbody></table>';

            $("#mention_history_div").append(table);
            load_chart('mention_timeline_div',mention_data);
            //mention_graph_div = "#mention_graph_div";
            //build_graph(mention_graph_div, user_history_list["mention_graph"]);

        });

    });


    function display_statistics(div,data){
        $("#user_statistics").empty();
        data = data.statistics
        var table = '<table id="user_statistics" data-toggle="table" class="table"><thead>' +
                     '<tr><th>Name</th><th>Value</th></thead>' +
                     '<tbody>'
                var table_body = $("#user_statistics").find("tbody");
                table += "<tr><td>Number of Unique Hashtags</td><td>"+ data["unique_hashtags"] + "</td></tr>";
                table += "<tr><td>Minimum Usage of a Hashtag</td><td>"+ data["min_amount_similar_hashtags"] + "</td></tr>";
                table += "<tr><td>Maximum Usage of a Hashtag</td><td>"+ data["max_amount_similar_hashtags"] + "</td></tr>";
                table += "<tr><td>Number of Unique Mentions</td><td>"+ data["unique_mentions"] + "</td></tr>";
                table += "<tr><td>Minimum Usage of a Mention</td><td>"+ data["min_amount_similar_mentions"] + "</td></tr>";
                table += "<tr><td>Maximum Usage of a Mention</td><td>"+ data["max_amount_similar_mentions"] + "</td></tr>";
                table += "<tr><td>Unique Hashtags</td><td>";
                for (var i=0; i<data["hashtags"].length; i++){
                    table += data["hashtags"][i] + "<br>";
                }
                table += "<tr><td>Unique Mentions</td><td>";
                 for (var i=0; i<data["mentions"].length; i++){
                    table += data["mentions"][i] + "<br>";
                }
                table +=  "</td></tr>";
                table += '</tbody></table>';
        $(div).append(table);


    }


    function load_chart(div, data){
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            var datatable = new google.visualization.DataTable();name
            datatable.addColumn('number', 'Step');
            datatable.addColumn('date', 'Time Datapoint');
            dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}})

            var timeline = [];
            for (var i=0; i<data.length; i++){
            date = new Date(data[i]["created_at"])
            var item = [i,date, date];
            datatable.addRow(item);
            }

            var options = {
            tooltip:  {isHtml: true},
            width : 1500,
            height: 800,
            title: 'Time Series Diagram',
            hAxis: {
              title: 'User\'s Tweet Identifier'
            },
            vAxis : {
              title: 'Date Range'
            }

            };

            var chart = new google.visualization.ScatterChart(document.getElementById(div));
            chart.draw(datatable, options);
            }
    }

});
