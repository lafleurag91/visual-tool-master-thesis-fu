$(function(){

    var globalTopics;


    function buildGraph(data){
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {
              var datatable = new google.visualization.DataTable();
              datatable.addColumn('string', 'Label');
              datatable.addColumn('number', 'Count');

              for (var i=0; i<data["topics"].length; i++){
                var item = [data["topics"][i], data["counts"][i]];
                datatable.addRow(item);
              }

              var options = {
                title: 'Top Topics',
                chartArea: {width: '50%'},
                hAxis: {
                  title: 'Occurrence Count',
                  minValue: 0
                },
                vAxis: {
                  title: 'Topics'
                }
              };

              var chart = new google.visualization.BarChart(document.getElementById('topic_ngram_diagram'));

              chart.draw(datatable, options);

              google.visualization.events.addListener(chart, 'select', selectHandler);

              function selectHandler(e) {
                var selection = chart.getSelection();
                var message = '';
                var item = selection[0];
                buildTextCorpus(globalTopics["texts"],item.row)
            }
        }
    }

    function buildTextCorpus(data, index){
        if ($("#topic_ngram_texts").children().length > 0){
            $("#topic_ngram_texts").empty();
        }
        var defaultItem = index;
        var listgroup = $("<ul></ul>").addClass("list-group");
        var itemValues = data[defaultItem];
        for (var i=0; i< itemValues.length; i++){
            useritem = $("<li></li>").addClass("list-group-item").text(itemValues[i]);
            listgroup.append(useritem);
        }
        $("#topic_ngram_texts").append(listgroup);
    }




     $.getJSON('/get_topics_ngrams', {}, function (topics) {
        globalTopics = topics;
        buildGraph(topics);
        buildTextCorpus(topics["texts"],0);
     });
});


