$(function(){
    var user_tweets_topics = []

    $("#profile_info").hide();
    $("#chart_title").hide();
    $("#suspended_user").hide();
    $("#bot").val(userid);
    $("#user").val(userid);
    $("#user_input_tab5").val(userid);



    var colors = [
                "#0000FF","#D3D3D3", "#BC8F8F","#FF0000","#A0522D",
                "#FFA500","#6B8E23","#7CFC00","#2E8B57","#20B2AA",
                "#008B8B","#00BFFF","#00FFFF","#4682B4","#6A5ACD",
                "#9932CC","#EE82EE","#FF00FF","#C71585","#FF1493",
                "#FFC0CB","#FFFF00","#FFDAB9", "#FA8072","#DAA520"
            ];



    function load_scores(id, score){
        /*if (user_profile[id] > 0.5){
                 $("#"+id).prev().addClass("critical");
        }*/
        if (user_profile[id] == -1){
            $("#"+id).parent().hide();
        }
        else{
            $("#"+id).append(user_profile[id]);
            $("#"+id).show();
        }
    }

    function load_topics(userid){

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            shapes = [
                "circle","triangle","square","diamond","star"
            ];

            var datatable = new google.visualization.DataTable();
            datatable.addColumn('number', 'x');
            datatable.addColumn('number', 'y');
            datatable.addColumn({'type': 'string', 'role': 'style'});
            datatable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

            for (var i=0; i<datapoints.length; i++){
                var topic = target_names[targets[i]];
                var label = "<b>User ID: "+sentences_per_user[i][0] + "</b> ("+topic+")";
                var color = topic.split("_")[1];
                var coloridx = parseInt(color) - 1;
                var color = colors[coloridx];
                //var shape = shapes[coloridx % shapes.length];
                var shape = "triangle";
                if (userid == sentences_per_user[i][0]){
                    var shape = "star";
                    //var color = "#000000";
                    var item = [datapoints[i][0],
                            datapoints[i][1],
                            'point {shape-type:'+shape+'; fill-color: ' + color +'; size: 20; stroke-color:#000000; stroke-width:5;}',
                            '<div style="width:300px;">'+label+'</div>'
                            ];
                }
                else {
                    var item = [datapoints[i][0],
                            datapoints[i][1],
                            'point {shape-type:'+shape+'; fill-color: ' + color +'; size: 3;}',
                            '<div style="width:300px;">'+label+'</div>'
                            ];
                }
                datatable.addRow(item);
            }

            var options = {
                title: 'TNSE Topic Distribution',
                width: 1700,
                height: 1000,
                focusTarget: 'datum',
                tooltip: { isHtml: true },
                legend: 'none'
            };

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                    var item = selection[i];
                    if (item.row != null && item.column != null) {
                        var str = datatable.getFormattedValue(item.row, item.column);
                        message += "User ID: "+sentences_per_user[item.row][0];
                    }
                }
                if (message == '') {
                    message = 'nothing';
                }
                alert(message);
            }

            var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

            chart.draw(datatable, options);
            google.visualization.events.addListener(chart, 'select', selectHandler);

        }

     }

    function fillTopics(){
            var div = $("#topic_top_10_words");
            for (var i=0; i < Object.keys(top_10_words_topic).length; i++){
                colidx = i % 5;
                var col = $("#col"+colidx);
                var new_row = $("<div></div>");
                new_row.addClass("row");
                var panel = $("<div></div>");
                panel.addClass("panel");
                panel.addClass("panel-default");
                panel.addClass("panel-toggle");
                var panelhead = $("<div></div>").addClass("panel-heading").css("background-color", colors[i]);
                if (colors[i] == "#0000FF"){
                    panelhead.css("color","white");
                }
                var btn = $("<button></button>");
                btn.addClass("topicbtn");
                btn.text("Topic"+(i+1));
                panelhead.append(btn);
                var panelbody = $("<div></div>");
                var ul = $("<ul></ul>");
                ul.addClass("list-group");
                for (var j=0; j<10; j++){
                    var li = $("<li></li>");
                    li.addClass("list-group-item");
                    li.addClass("no-border");
                    li.text(top_10_words_topic[i][j]);
                    ul.append(li);
                }
                panelbody.append(ul);
                panel.append(panelhead);
                panel.append(panelbody);
                col.append(panel);
            }
            div.append(col);
        }
    function load_bot_chart(userid, userlabels){

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        botcolors = [
                "#FF0000","#008000"
            ];

        function drawChart() {
            var datatable = new google.visualization.DataTable();
            datatable.addColumn('number', 'x');
            datatable.addColumn('number', 'y');
            datatable.addColumn({'type': 'string', 'role': 'style'});
            datatable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

            for (var i=0; i<datapoints.length; i++){
                var topic = target_names[targets[i]];
                var uid = sentences_per_user[i][0];
                var botlabel = userlabels[uid];
                var bot;
                if (botlabel == 1){
                    color = botcolors [0];
                    bot = "Bot";
                }
                else {
                    color = botcolors [1];
                    bot ="User";
                }
                var label = "<b>User ID: "+ sentences_per_user[i][0] + "</b>";
                var shape = "triangle";
                if (userid == sentences_per_user[i][0]){
                    var shape = "star";
                    var item = [datapoints[i][0],
                            datapoints[i][1],
                            'point {shape-type:'+shape+'; fill-color: black; size: 20; stroke-color:#000000; stroke-width:5;}',
                            '<div style="width:300px;">'+label+'</div>'
                            ];
                }
                else {
                    var item = [datapoints[i][0],
                            datapoints[i][1],
                            'point {shape-type:'+shape+'; fill-color: ' + color +'; size: 3;}',
                            '<div style="width:300px;">'+label+'</div>'
                            ];
                }
                datatable.addRow(item);
            }

            var options = {
                width: 1700,
                height: 1000,
                focusTarget: 'datum',
                tooltip: { isHtml: true },
                legend: 'none'
            };

            function selectHandler() {
                var selection = chart.getSelection();
                var message = '';
                for (var i = 0; i < selection.length; i++) {
                    var item = selection[i];
                    if (item.row != null && item.column != null) {
                        var str = datatable.getFormattedValue(item.row, item.column);
                        message += "User ID: "+sentences_per_user[item.row][0];
                    }
                }
                if (message == '') {
                    message = 'nothing';
                }
                alert(message);
            }

            var chart = new google.visualization.ScatterChart(document.getElementById('bot_chart'));
            chart.draw(datatable, options);
            google.visualization.events.addListener(chart, 'select', selectHandler);
        }
    }


    function comp(a,b){
        return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
    }

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function load_chart(div, data){
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            var datatable = new google.visualization.DataTable();
            datatable.addColumn('date', 'Time Datapoint');
            datatable.addColumn('number', 'Step');
            datatable.addColumn({'type': 'string', 'role': 'style'});
            datatable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}})

            var timeline = [];
            for (var i=0; i<data.length; i++){
                date = new Date(data[i]["created_at"]);
                day = addZero(date.getDate());
                month = addZero(date.getMonth() +1);
                year = date.getFullYear();
                hours = addZero(date.getHours());
                minutes = addZero(date.getMinutes());
                seconds = addZero(date.getSeconds());

                formatted = day + "." +month+"."+year+" "+hours+":"+minutes+":"+seconds;

                var item = [date,
                            i,
                            "point {shape-type: diamond; fill-color: #337ab7; size: 5;}",
                            formatted]
                datatable.addRow(item);
            }

            var options = {
                tooltip:  {isHtml: true},
                width : 1700,
                height: 800,
                title: 'Time Series Diagram',
                hAxis: {
                  title: 'Date Range'
                },
                vAxis : {
                  title: 'User\'s Tweet Identifier'
                },
                legend:{position: 'none'}
            };

            var chart = new google.visualization.ScatterChart(document.getElementById(div));
            chart.draw(datatable, options);
            }
    }

    fillTopics();


    $(document).on("click","button.topicbtn",function(e){
        $("#topic_tweets").empty();
        $("#show_tweets").empty();
        var selected_topic = $(this).text();
        $("#selected_topic").html("Tweets that correpond to "+selected_topic);
        selected_topic = (parseInt(selected_topic.split("Topic")[1]));
        $.getJSON('/load_tweets_per_topic', {topicid:selected_topic}, function (tweets) {
            user_tweets_topics = tweets["data"]
            tweets_list = tweets["data"];
            for (var i=0; i<tweets_list.length; i++){
                var name = $("<div></div>");
                name.text(tweets_list[i]["screen_name"]);
                var topic_prob = $("<div></div>");
                topic_prob.addClass("progress-bar");
                topic_prob.css("width",tweets_list[i]["topic_distribution"]+"%");
                topic_prob.text(tweets_list[i]["topic_distribution"]+"%");
                var div = $("<div></div>");
                div.addClass("progress");
                div.css("float","left");
                div.css("width","200px");
                div.append(topic_prob);
                var showButton = $("<button></button>");
                showButton.addClass("showDetails");
                showButton.text("?");
                showButton.attr("id",i);
                showButton.css("clear","left");

                var li = $("<li></li>").addClass("list-group-item");
                li.append(name);
                li.append(div);
                li.append(showButton);
                $("#topic_tweets").append(li);
            }
        });
     });

     $(document).on("click","button.showDetails",function(e){
        $("#show_tweets").empty();
        var button_i = $(this).attr("id");
        tweets_list = user_tweets_topics[button_i]["tweets"];
        var tweet_ul = $("<ul></ul>");
        tweet_ul.addClass("list-group");
        for (var i=0; i<tweets_list.length; i++){
            var li = $("<li></li>");
            li.addClass("list-group-item");
            li.text(tweets_list[i]);
            tweet_ul.append(li);
        }
        $("#show_tweets").append(tweet_ul);
     });

    $("#botometer_img").click(function () {
        $('#myModal').modal('show');
    });


    $("#user_search_tab5").on("click", function(e){
        $(".panel-collapse").removeClass("in");

        $("#profile_pic").empty();
        $("#name").empty();
        $("#screen_name").empty();
        $("#description").empty();
        $("#url").empty();
        $("#created_at").empty();
        $("#follower").empty();
        $("#following").empty();
        $("#profile_tweets").empty();
        $("#mean_mins_btw_tweets").empty();
        $("#max_mins_btw_tweets").empty();
        $("#min_mins_btw_tweets").empty();
        $("#mean_nr_words").empty();
        $("#min_nr_words").empty();
        $("#max_nr_words").empty();


        $("#friendscore").empty();
        $("#friendscore").attr("style","color:black");
        //$("#friendscore").prev().removeClass("critical");

        $("#networkscore").empty();
        $("#networkscore").attr("style","color:black");
        //$("#networkscore").prev().removeClass("critical");

        $("#userscore").empty();
        $("#userscore").attr("style","color:black");
        //$("#userscore").prev().removeClass("critical");

        $("#temporalscore").empty();
        $("#temporalscore").attr("style","color:black");
        //$("#temporalscore").prev().removeClass("critical");

        $("#contentscore").empty();
        $("#contentscore").attr("style","color:black");
        //$("#contentscore").prev().removeClass("critical");

        $("#botscore").empty();
        $("#botscore").attr("style","color:black");
        //$("#botscore").prev().removeClass("critical");


        $("#profile_info").hide();
        $("#chart_title").hide();
        user_input = $("#user_input_tab5").val();
        $.getJSON('/user_timeline', {user:user_input}, function (user_history_list) {
            user_history = user_history_list["data"];
            userid = user_history_list["userid"];
            userlabels = user_history_list["userlabels"];
            user_history = JSON.parse(user_history);
            user_history.sort(comp);
            $("#timeline_div_tab5").empty();
            load_chart('timeline_div_tab5',user_history);

            for (var i=0; i<user_history.length; i++){
                li = $("<li></li>").addClass("list-group-item");
                date = $("<div></div>").addClass("marginright").append("<b>"+user_history[i]["created_at"] +"</b>");
                li.append(date)
                li.append(user_history[i]["text"]);
                $("#profile_tweets").append(li);
            }

            $("#hashtags").empty();
            $("#unique_hashtags").empty();
            $("#max_amount_similar_hashtags").empty();
            $("#min_amount_similar_hashtags").empty();
            $("#min_amount_similar_mentions").empty();

            $("#mentions").empty();
            $("#unique_mentions").empty();
            $("#max_amount_similar_mentions").empty();

            var statistics = user_history_list["statistics"];
            var hashtags_list = statistics["hashtags"];
            if (hashtags_list.length == 0){
                var button = $("<button></button>");
                button.addClass("btn");
                button.addClass("btn-danger");
                button.attr("aria-disabled",true);
                button.html("No Hashtags Used...");
                $("#hashtags").append(button);
            }
            else{
                for (var i=0; i<hashtags_list.length; i++){
                    var button = $("<button></button>");
                    button.addClass("btn");
                    button.addClass("btn-info");
                    button.attr("aria-disabled",true);
                    button.html("#"+hashtags_list[i]);
                    $("#hashtags").append(button);
                }
            }
            $("#unique_hashtags").append(statistics["unique_hashtags"]);
            $("#max_amount_similar_hashtags").append(statistics["max_amount_similar_hashtags"]);
            $("#min_amount_similar_hashtags").append(statistics["min_amount_similar_hashtags"]);


            var mentions_list = statistics["mentions"];
            if (mentions_list.length == 0){
                var button = $("<button></button>");
                button.addClass("btn");
                button.addClass("btn-danger");
                button.attr("aria-disabled",true);
                button.html("No Mentions Used...");
                $("#mentions").append(button);
            }
            else{
                for (var i=0; i<mentions_list.length; i++){
                    var button = $("<button></button>");
                    button.addClass("btn");
                    button.addClass("btn-info");
                    button.attr("aria-disabled",true);
                    button.html("@"+mentions_list[i]);
                    $("#mentions").append(button);
                }
            }
            $("#unique_mentions").append(statistics["unique_mentions"]);
            $("#max_amount_similar_mentions").append(statistics["max_amount_similar_mentions"]);
            $("#min_amount_similar_mentions").append(statistics["min_amount_similar_mentions"]);


            $("#profile_info").show();
            $("#collapse1_tab5").addClass("in");
            $("#chart_title").show();
            load_topics(userid);
            load_bot_chart(userid, userlabels)
        });


        $.getJSON('/user_profile', {user:user_input}, function(user_profile_data){
            user_profile = user_profile_data["data"];
            img = $("<img></img>").attr("src",user_profile["profile_img"]).attr("alt","Image cannot be displayed").attr("onerror","this.src='https://marketingland.com/wp-content/ml-loads/2013/05/twitter-egg-no.jpg'");
            $("#profile_pic").append(img);
            $("#name").append("<b>"+ user_profile["name"]+ "</b>");
            $("#screen_name").append("@"+user_profile["screenname"]);
            $("#description").append(user_profile["description"]);
            link = $("<a></a>").attr("href",user_profile["url"]);
            link.append(user_profile["url"]);
            $("#url").append(link);
            $("#created_at").append(user_profile["created_at"]);
            $("#follower").append(user_profile["followers"]);
            $("#following").append(user_profile["following"]);

            $("#mean_mins_btw_tweets").append(user_profile["mean_mins_btw_tweets"]);
            $("#max_mins_btw_tweets").append(user_profile["max_mins_btw_tweets"]);
            $("#min_mins_btw_tweets").append(user_profile["min_mins_btw_tweets"]);

            $("#mean_nr_words").append(user_profile["mean_nr_words"]);
            $("#min_nr_words").append(user_profile["min_nr_words"]);
            $("#max_nr_words").append(user_profile["max_nr_words"]);

            load_scores("friendscore", user_profile["friendscore"]);
            load_scores("networkscore", user_profile["networkscore"]);
            load_scores("userscore", user_profile["userscore"]);
            load_scores("temporalscore", user_profile["temporalscore"]);
            load_scores("contentscore", user_profile["contentscore"]);
            load_scores("botscore", user_profile["botscore"]);

            if (user_profile["friendscore"] == -1){
                $("#suspended_user").show();
            }
            else {
                $("#suspended_user").hide();
            }
        })

    });
});