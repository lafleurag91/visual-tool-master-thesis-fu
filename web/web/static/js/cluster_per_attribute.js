$(function(){

    function load_chart(div, data, centroids, labels, userids, axislabels, datasetlabels){


        $("#"+div).empty();
        google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

              var datatable = new google.visualization.DataTable();
              for (var z=0; z<data[0].length; z++){
                datatable.addColumn('number', '');
              }
              datatable.addColumn({type: 'string', role:'style'});
              datatable.addColumn({type: 'string', role: 'tooltip'});

              var colors = ["green", "red", "yellow", "orange", "blue", "pink", "lightblue", "magenta", "grey", "brown"];
              var series = {}
              for (var i=0; i<data.length; i++){
                var item = data[i];
                var rowcontent = [];
                for (var j=0; j<item.length; j++){
                    rowcontent.push(parseInt(item[j]));
                }
                var label = labels[i];
                var color = colors[label];
                if (typeof datasetlabels != "undefined"){
                    if (datasetlabels[i] == "user"){
                        rowcontent.push("point { size: 5; shape-type: star; fill-color: " + color +"; }");
                    }
                    else {
                        rowcontent.push("point { size: 5; shape-type: triangle; fill-color: " + color +"; }");
                    }
                }
                else {
                    rowcontent.push("point { size: 5;  fill-color: " + color +"; }");
                }
                rowcontent.push("User ID: "+ userids[i] + " ("+rowcontent[0]+", "+rowcontent[1]+")");
                datatable.addRow(rowcontent);
              }
              num_data_inserted = data.length;
              for (var j=0; j<centroids.length; j++){
                    var centroid = centroids[j];
                    var rowcontent = [];
                    for (var z=0; z<centroid.length; z++){
                        rowcontent.push(parseInt(centroid[z]));
                    }
                    rowcontent.push("point { size: 8; shape-type: star; fill-color: black; }");
                    rowcontent.push("Cluster Center at: " + rowcontent[0]+", "+rowcontent[1]);
                    datatable.addRow(rowcontent);
              }

              var options = {
                width : 2000,
                height: 800,
                title: "K-MEANS Cluster Visulaization",
                vAxis: {
                    title: axislabels[1]
                },
                hAxis: {
                    title: axislabels[0]
                },
                legend: {position: "none"},
                tooltip: {
                    trigger : "both"
                }


              };

              var chart = new google.visualization.ScatterChart(document.getElementById(div));

              chart.draw(datatable, options);
        }
    }


     $.getJSON('/get_available_attributes', {}, function (data) {
        var attribute_list = data["attributes"];
        var container = $("#available_attributes_list");

        var attrlist = $("<ul></ul>").addClass("list-group checked-list-box");
        for (var i=0; i< attribute_list.length; i++){
            attribute_item = $("<li></li>").addClass("list-group-item").text(attribute_list[i]);
            attrlist.append(attribute_item);
        }
        container.append(attrlist);
        var button= $("<button></button>").addClass("btn btn-primary col-xs-12");
        button.attr("id","get_checked_data").text("Get checked data");
        var k = $("<input></input>").attr("id","num_clusters").attr("type","text").attr("placeholder","Number of Clusters").addClass("col-xs-12");
        container.append("<br></br>").append(k);
        container.append("<br></br>").append(button);


        $('.list-group.checked-list-box .list-group-item').each(function () {
            var $widget = $(this),
                $checkbox = $('<input type="checkbox" class="hidden" />'),
                color = ($widget.data('color') ? $widget.data('color') : "primary"),
                style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };
            $widget.css('cursor', 'pointer')
            $widget.append($checkbox);

            $widget.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                $widget.data('state', (isChecked) ? "on" : "off");
                $widget.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$widget.data('state')].icon);
                if (isChecked) {
                    $widget.addClass(style + color + ' active');
                } else {
                    $widget.removeClass(style + color + ' active');
                }
            }

            function init() {

                if ($widget.data('checked') == true) {
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                }

                updateDisplay();

                if ($widget.find('.state-icon').length == 0) {
                    $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
                }
            }
            init();
        });

        $('#get_checked_data').on('click', function(event) {
            event.preventDefault();
            var checkedItems = [], counter= 0;
            $(".checked-list-box li.active").each(function(idx, li) {
                checkedItems[counter] = $(li).text();
                counter++;
            });
            var k = $("#num_clusters").val();
            $.getJSON('/cluster_per_attributes', {data:  JSON.stringify(checkedItems), num_clusters : k}, function (data) {
                 var datapoints = data["data"];
                 var centroids = data["centroids"];
                 var labels = data["labels"];
                 var userids = data["user_ids"];
                 var dataset_labels = data["dataset_labels"]
                 load_chart("cluster_div", datapoints, centroids, labels, userids, checkedItems, dataset_labels)
            });
        });
    });
});


