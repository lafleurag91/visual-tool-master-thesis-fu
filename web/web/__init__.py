from flask import Flask, render_template, request, redirect, url_for
import pandas as pd
import re
import os
import numpy as np
from sklearn.cluster import KMeans
import web.web.graph_structure
#import botometer
import tweepy
import json
import networkx
import torch
from collections import Counter
import pickle
from sklearn.model_selection import train_test_split
import random
import datetime
import time

class BotOrNot():

    bon = None

    def __init__(self, consumerKey, consumerSecret, accessToken, accessTokenSecret):
        mashape_key="1J3uV9JM3KmshY7tmDwelzCZ8SVdp1lI3Ghjsn1kqwW4UOajau"
        twitter_app_auth = {
            'consumer_key': consumerKey,
            'consumer_secret': consumerSecret,
            'access_token': accessToken,
            'access_token_secret': accessTokenSecret,
        }
        #self.bon = botometer.Botometer(mashape_key=mashape_key, wait_on_ratelimit=True, **twitter_app_auth)

    def check_users(self, user_list):
        #results = list(self.bon.check_accounts_in(user_list))
        return []#results


consumerKey = "f1extHsSnutNDhQdvdYLTnUZP"
consumerSecret = "r4jTRVjkao4wknlIDI6riYVScBuLbkRGycw6gTZbALCHeudN8Y"
accessToken = "738295264226750464-75hWmV7iEsijhfa6DJaPa0ehZ98wTVT"
accessTokenSecret = "GPUTqgcaGH0fV3tx3bYUA5xXiFCrbHZJLo6vytg5umC0N"

app = Flask(__name__)
tweet_data = None
grouped_tweet_data = None
user_data = None
mentioned_user_names = None
bom = BotOrNot(consumerKey, consumerSecret, accessToken, accessTokenSecret)

SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
group = None
decisions_file = None
decisions = None
possible_bots_to_test = ["223769410", "42843646", "1336218432", "300989278", "301294515"]#, "3128919813", "421403288", "711236259004465156", "823485329613225984" ]
possible_real_users_to_test = ["19889079","34888442", "41080992", "24859163", "851470576938090497"]#, "728326774027919360", "766560365098311681", "828127693501784064", "4722931887", "428445865", "209054410"]

user_list = ["223769410", "19889079", "42843646", "34888442", "1336218432", "41080992", "3128919813", "24859163", "1238447220", "194538182"]
step = 0
start = 0
num_users = 0


"""
 UTILITY FUNCTIONS.html
"""
def load_tweet_data(filename ="~/GIT/lda2vec/examples/twitter/lda2vec/tweets_shuffled_no_links.csv", delimiter="\t", names=["user_id", "tweet_id", "tweet", "created_at", "tweet_no_links"],
                    header_idx = 0, datatype=str, nrows = 1000000):
    if nrows:
        data = pd.read_csv(filepath_or_buffer=filename, delimiter=delimiter,
                       header=header_idx, names=names, dtype=datatype,
                       nrows=nrows)
    else:
        data = pd.read_csv(filepath_or_buffer=filename, delimiter=delimiter,
                           header=header_idx, names=names, dtype=datatype)
    return data

def load_user_data(filename="~/GIT/lda2vec/examples/twitter/data/legitimate_users.txt", delimiter="\t",
                   names=["user_id","created_at","collected_at","number_of_followings","number_of_followers","number_of_tweets","length_of_screenname ","length_of_description"],
                   header_idx=None, datatype=str, label=None):

    data = pd.read_csv(filepath_or_buffer=filename, delimiter=delimiter, header=header_idx, names=names, dtype=datatype)
    data.drop("collected_at", axis=1, inplace=True)
    data["created_at"] = data["created_at"].apply(lambda d: date_string_to_number(d))
    if not label is None:
        data["label"] = label
    return data


def user_mention_graph(user,user_df):

    """global mentioned_user_names
    if mentioned_user_names is None:
        with open("~/GIT/lda2vec/examples/twitter/lda2vec/mentioned_user_names.txt", "rb") as f:
            mentioned_user_names = pickle.load(f)"""

    tweets = tweet(user_df).values
    tweets = " ".join(tweets)
    mentions = re.findall(r"@(\w+)", tweets)
    mentions = set(mentions)

    json_link_list = []
    for i,mention in enumerate(mentions):
        json_link_list.append({"source":0, "target":i+1, "label_source":str(user),"label_target":str(mention), "type":"suit"})
    graph = {"links":json_link_list}
    return graph


def statistics(user_tweet_df):

    def extract_mentions(tweet):
        import re
        mentions = re.findall(r"@(\w+)", tweet)
        return mentions

    def extract_hashtags(tweet):
        import re
        mentions = re.findall(r"#(\w+)", tweet)
        return mentions

    hashtags = {}
    mentions = {}
    for t in tweet(user_tweet_df).values:
        extr_hashtags = extract_hashtags(t)
        extr_mentions = extract_mentions(t)
        for extr_h in extr_hashtags:
            if extr_h in hashtags:
                amount = hashtags[extr_h]
                hashtags[extr_h] = amount+1
            else:
                hashtags[extr_h] = 1

        for extr_m in extr_mentions:
            if extr_m in mentions:
                amount = mentions[extr_m]
                mentions[extr_m] = amount+1
            else:
                mentions[extr_m] = 1

    amount_unique_hashtags = len(hashtags.keys())
    amount_unique_mentions = len(mentions.keys())
    hashtag_values = list(hashtags.values())
    mention_values = list(mentions.values())

    sorted_hashtags = []
    for w in sorted(hashtags, key=hashtags.get, reverse=True):
        sorted_hashtags.append(w)

    sorted_mentions = []
    for w in sorted(mentions, key=mentions.get, reverse=True):
        sorted_mentions.append(w)



    max_length_list = 20

    if len(sorted_hashtags) > max_length_list:
        top_sorted_hashtags = sorted_hashtags[:max_length_list]
    else:
        top_sorted_hashtags = sorted_hashtags

    if len(sorted_mentions) > max_length_list:
        top_sorted_mentions = sorted_mentions[:max_length_list]
    else:
        top_sorted_mentions = sorted_mentions

    max_amount_similar_hashtags = np.amax(hashtag_values) if len(hashtag_values)>0 else 0
    print("max_amount_similar_hashtags",max_amount_similar_hashtags)
    max_amount_similar_mentions = np.amax(mention_values) if len(mention_values)>0 else 0
    print("max_amount_similar_mentions",max_amount_similar_mentions)
    min_amount_similar_hashtags = np.amin(hashtag_values) if len(hashtag_values)>0 else 0
    print("min_amount_similar_hashtags",min_amount_similar_hashtags)
    min_amount_similar_mentions = np.amin(mention_values) if len(mention_values)>0 else 0
    print("min_amount_similar_mentions",min_amount_similar_mentions)

    statistics = {"unique_hashtags":str(amount_unique_hashtags),
                  "unique_mentions":str(amount_unique_mentions),
                  "max_amount_similar_hashtags": str(max_amount_similar_hashtags),
                  "max_amount_similar_mentions":str(max_amount_similar_mentions),
                  "min_amount_similar_hashtags":str(min_amount_similar_hashtags),
                  "min_amount_similar_mentions":str(min_amount_similar_mentions),
                  "hashtags":top_sorted_hashtags,
                  "mentions":top_sorted_mentions
    }
    return statistics

"""
HTML PAGE RENDERERS
"""

@app.route("/")
def index():
    global group, tweet_data, user_data, decisions_file, decisions, user_list, step, num_users
    #group = random.randint(0,1)
    group = num_users % 2
    num_users += 1
    tweet_data_file = os.path.join(SITE_ROOT, "static/data", "tweets_politics_df_reloaded")
    user_data_file = os.path.join(SITE_ROOT, "static/data", "user_politics_df_2_topics")
    tweet_data = pd.read_pickle(tweet_data_file)
    user_data = pd.read_pickle(user_data_file)

    step = 0
    date = datetime.datetime.now().strftime("%d_%m_%y_%H:%M:%S")
    decisions_file = str(date+"_group_"+str(group))
    print(decisions_file)
    decisions_file = os.path.join(SITE_ROOT, "static/data", decisions_file)
    decisions = pd.DataFrame(columns=["userid","decision","group", "time"])
    print("Gruppe "+str(group))
    if group == 0:
        return render_template("group1.html")
    else:
        return render_template("group2.html")


@app.route("/tool")
def start_tool():
    global group, start
    global tweet_data, user_data, user_list, step
    start = time.time()
    uid=user_list[step]
    if group == 0:
        return load_topics(uid)
    else:
        screenname = user_data[userid(user_data) == uid].screen_name.values[0]
        user_link = "https://twitter.com/search?l=&q=from%3A" + screenname + "%20since%3A2017-05-31%20until%3A2017-10-14&src=typd&lang=en"
        return render_template("twitter_load.html", user_link=json.dumps(user_link), userid  = uid)

@app.route("/decision", methods = ['POST'])
def decision():
    submit_btn = request.form
    global decisions, group, start, step, user_data
    tmp = time.time()
    time_for_decision = tmp - start
    if  "legitimate" in submit_btn:
        decisions = decisions.append({"userid": submit_btn["legitimate"], "decision": 0, "group": group, "time": time_for_decision}, ignore_index=True)
    else:
        decisions = decisions.append({"userid": submit_btn["bot"], "decision": 1, "group": group, "time": time_for_decision}, ignore_index=True)
    decisions.to_pickle(decisions_file)

    if step < len(user_list)-1:
        step+=1
        uid = user_list[step]
    else:
        #stop it
        decisions.to_pickle(decisions_file)
        return render_template("end.html")

    #load_next user
    start = time.time()
    if group == 0:
        return load_topics(uid)
    else:
        #create link
        screenname = user_data[userid(user_data) == uid].screen_name.values[0]
        user_link = "https://twitter.com/search?l=&q=from%3A"+ screenname +"%20since%3A2017-05-31%20until%3A2017-10-14&src=typd&lang=en"
        return render_template("twitter_load.html", user_link=json.dumps(user_link), userid=uid)


@app.route("/questionnaire", methods=['POST'])
def questionnaire():
    options = request.form
    question1 = options["q1"]
    question2 = options["q2"]
    question3 = options["q3"]
    question4 = options["q4"]

    global decisions
    decisions["answers"] = question1 + question2 + question3 + question4
    decisions.to_pickle(decisions_file)
    return render_template("end_2.html")


def load_topics(uid):
    model_file = os.path.join(SITE_ROOT, "static/data", "model_state.pytorch")

    state = torch.load(model_file, map_location=lambda storage, loc: storage)

    doc_weights = state['doc_weights.weight'].cpu().clone().numpy()
    topic_vectors = state['topics.topic_vectors'].cpu().clone().numpy()
    resulted_word_vectors = state['neg.embedding.weight'].cpu().clone().numpy()

    # distribution over the topics for each document
    topic_dist = softmax(doc_weights)

    # vector representation of the documents
    doc_vecs = np.matmul(topic_dist, topic_vectors)

    doc_decoder_file = os.path.join(SITE_ROOT, "static/data", "doc_decoder_twitter.npy")
    doc_decoder = np.load(doc_decoder_file)[()]

    decoder_file = os.path.join(SITE_ROOT, "static/data", "decoder_twitter.npy")
    decoder = np.load(decoder_file)[()]

    sentence_per_user_file = os.path.join(SITE_ROOT, "static/data", "sentences_per_user")
    with open(sentence_per_user_file, "rb") as fp:
        sentences_per_user = pickle.load(fp)
    docs = [sent for (user, sent) in sentences_per_user]

    # store each document with an initial id
    docs = [(i, doc) for i, doc in enumerate(docs)]

    TSNE_vis_data = os.path.join(SITE_ROOT, "static/data", "TSNE_data")
    with open(TSNE_vis_data, "rb") as fp:
        Y = pickle.load(fp)

    target_names = ['topic_' + str(i + 1) for i in range(25)]
    targets = np.zeros(shape=len(docs))
    for i in range(len(topic_dist)):
        max_topic = np.argmax(topic_dist[i])
        original_doc_id = doc_decoder[i]
        targets[original_doc_id] = max_topic
    len(targets)

    targets = np.array([targets[doc_decoder[i]] for i in range(len(doc_decoder))])

    similarity = np.matmul(topic_vectors, resulted_word_vectors.T)
    most_similar = similarity.argsort(axis=1)[:, -10:]
    topics = []

    for j in range(25):
        top_10_words_topic = [decoder[i] for i in reversed(most_similar[j])]
        topics.append(top_10_words_topic)

    docs_per_user = []
    docs_per_user_file = os.path.join(SITE_ROOT, "static/data", "docs_per_user_encoded")
    try:
        with open(docs_per_user_file, "rb") as fp:
            docs_per_user = pickle.load(fp)
    except:
        print("Creating encoded docs_per_user file...")
        for i in range(len(doc_decoder)):
            user_docs = [(j,doc) for j,doc in docs if j == doc_decoder[i]][0]
            (userid, tweets) = sentences_per_user[user_docs[0]]
            count_tweets = Counter(tweets.split(" "))
            top_10_words_user = []
            for i,w in enumerate(sorted(count_tweets, key=count_tweets.get, reverse=True)):
                if i < 10:
                    top_10_words_user.append(w + " ("+str(count_tweets[w])+")<br>")
            docs_per_user.append([userid, " ".join(top_10_words_user)])

        with open(docs_per_user_file, "wb") as fp:
            pickle.dump(docs_per_user, fp)

    return render_template('topics.html',
                           datapoints = Y.tolist(),
                           targets = targets.tolist(),
                           target_names = json.dumps(target_names),
                           docs_per_user = docs_per_user,
                           top_10_words_topic = topics,
                           userid = uid
                           )


@app.route("/cluster_per_attribute")
def load_cluster_per_attribute():
    return render_template("cluster_per_attribute.html")


@app.route("/user_timeline")
def user_timeline():
    try:
        user = json.loads(request.args.get('user'))
    except:
        user = request.args.get('user')
    print(user)
    if type(user) == str:
        # we have a screenname and need to get corresponding id first
        user_profile = user_data[user_data.screen_name == user]
        user = userid(user_profile).values[0]
    global tweet_data
    if tweet_data is None:
        tweet_data =load_tweet_data()

    print("grouping data")
    global grouped_tweet_data
    grouped_tweet_data = groupby_userid(tweet_data)
    values_of_group = grouped_tweet_data.get_group(str(user))

    json_values_of_group = values_of_group.to_json(orient='records')

    userlabels_file = os.path.join(SITE_ROOT, "static/data", "userlabels")
    try:
        with open(userlabels_file, "rb") as fp:
            userlabels = pickle.load(fp)
    except:
        # load mapping now.
        userlabels = {}
        user_label_view = user_data[["userid","label"]]
        for idx, row in user_label_view.iterrows():
            userlabels[userid(row)] = row.label

        with open(userlabels_file, "wb") as fp:
                pickle.dump(userlabels, fp)

    stats = statistics(values_of_group)
    return json.dumps({"data":json_values_of_group, "userid":user, "userlabels":userlabels, "statistics" : stats})



@app.route("/load_tweets_per_topic")
def load_tweets_per_topic():
    topicid = request.args.get('topicid')
    global user_data, tweet_data
    user_topic_df = user_data[["userid","screen_name","topic_"+str(topicid)]]
    user_topic_df = user_topic_df.sort_values(by=["topic_"+str(topicid)], ascending=False)
    top_10_topic_df = user_topic_df.head(10)
    topic_tweets = []
    for idx, row in top_10_topic_df.iterrows():
        tweet_user_df = tweet_data[userid(tweet_data) == userid(row)]
        topic_tweet = tweet(tweet_user_df)
        topic_tweets.append({"screen_name": row.screen_name, "topic_distribution":(row["topic_"+str(topicid)])*100, "tweets":list(topic_tweet.values)})
    return json.dumps({"data": topic_tweets })


@app.route("/user_profile")
def user_profile():
    try:
        user = json.loads(request.args.get('user'))
    except:
        user = request.args.get('user')
    global user_data
    if type(user) == str:
        # we have a screenname and need to get corresponding id first
        user_profile = user_data[user_data.screen_name == user]
        user = userid(user_profile).values[0]
    user_profile = user_data[userid(user_data) == str(user)]
    profile_pic = user_profile.profile_img.values[0]
    ending_idx = profile_pic.rfind(".")
    ending = profile_pic[ending_idx:]
    idx = profile_pic.rfind("_")
    profile_pic = profile_pic[:idx+1]
    profile_pic += "400x400"+ending

    descr = user_profile.description.values[0]
    if descr == "nan":
        descr = "-"
    url = user_profile.url.values[0]
    if url == "nan":
        url = "-"
    user_profile_dict = {}
    user_profile_dict["name"] = user_profile.name.values[0]
    user_profile_dict["screenname"] = user_profile.screen_name.values[0]
    user_profile_dict["description"] = descr
    user_profile_dict["url"] = url
    user_profile_dict["created_at"] = user_profile.created_at.values[0]
    user_profile_dict["followers"] = user_profile.follow_count.values[0]
    user_profile_dict["following"] = user_profile.friends_count.values[0]
    user_profile_dict["profile_img"] = profile_pic


    user_profile_dict["mean_mins_btw_tweets"] = str(int(user_profile.mean_seconds_between_tweets.values[0]/60))

    min_secs = user_profile.min_seconds_between_tweets.values[0]
    if int(min_secs / 60) != 0:
        user_profile_dict["min_mins_btw_tweets"] = str(int(min_secs))
    else:
        user_profile_dict["min_mins_btw_tweets"] = str(float(min_secs/60))


    user_profile_dict["max_mins_btw_tweets"] = str(int(user_profile.max_seconds_between_tweets.values[0]/60))
    user_profile_dict["mean_nr_words"] = str(int(user_profile.mean_nr_words.values[0]))
    user_profile_dict["min_nr_words"] = str(int(user_profile.min_nr_words.values[0]))
    user_profile_dict["max_nr_words"] = str(int(user_profile.max_nr_words.values[0]))
    try:
        # if score not yet calculated, calculate them now.
        user_profile_dict["friendscore"] = user_profile.friendscore.values[0]
        user_profile_dict["networkscore"] = user_profile.networkscore.values[0]
        user_profile_dict["userscore"] = user_profile.userscore.values[0]
        user_profile_dict["sentimentscore"] = user_profile.sentimentscore.values[0]
        user_profile_dict["temporalscore"] = user_profile.temporalscore.values[0]
        user_profile_dict["contentscore"] = user_profile.contentscore.values[0]
        user_profile_dict["botscore"] = user_profile.botscore.values[0]
    except:
        """
        result = bom.check_users([user])
        botscore = result[0][1]
        try:
            # if service returns an error message, like user not found, we set the botscore to 1 and all other to -1 as an indicator for such occurrence.
            user_profile_dict["friendscore"] = botscore["categories"]["friend"]
            user_profile_dict["networkscore"] = botscore["categories"]["network"]
            user_profile_dict["userscore"] = botscore["categories"]["user"]
            user_profile_dict["sentimentscore"] = botscore["categories"]["sentiment"]
            user_profile_dict["temporalscore"] = botscore["categories"]["temporal"]
            user_profile_dict["contentscore"] = botscore["categories"]["content"]
            user_profile_dict["botscore"] = botscore["scores"]["universal"]
        except:
            user_profile_dict["friendscore"] = -1
            user_profile_dict["networkscore"] = -1
            user_profile_dict["userscore"] = -1
            user_profile_dict["sentimentscore"] = -1
            user_profile_dict["temporalscore"] = -1
            user_profile_dict["contentscore"] = -1
            user_profile_dict["botscore"] = 1.0
        """
        pass
    return json.dumps({
        "data" : user_profile_dict
    })

def load_retweet_data(user_id):
    global tweet_data
    tweet_ids = tweet_data[userid(tweet_data) == user_id].tweetid.values

    auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(accessToken, accessTokenSecret)
    api = tweepy.API(auth, wait_on_rate_limit=True)

    retweet_users = {}
    normal_tweets = []
    for tweet_id in tweet_ids:
        try:
            tweet = api.get_status(tweet_id)
            rt_status = tweet.retweeted_status
            retweet_user = rt_status.user.id_str

            if not retweet_user in retweet_users:
                retweet_users[retweet_user] = [tweet]
            else:
                tweet_list = retweet_users[retweet_user]
                tweet_list.append(tweet)
                retweet_users[retweet_user] = tweet_list
        except:
            #no retweet - treat as normal tweet
            tweet_obj = tweet_data[tweetid(tweet_data) == tweet_id]
            normal_tweets.append(tweet_obj)
            continue

    return retweet_users, normal_tweets

@app.route("/search_hashtag")
def search_hashtag():
    hashtag = request.args.get('hashtag')
    print(hashtag)
    global tweet_data
    if tweet_data is None:
        tweet_data = load_tweet_data()
    hashtag_data = tweet_data[tweet(tweet_data).str.contains(hashtag)]
    json_hashtag_data = hashtag_data.to_json(orient="records")
    return json.dumps({"data": json_hashtag_data})

@app.route("/search_mention")
def search_mention():
    mention = request.args.get('mention')
    print(mention)
    global tweet_data
    if tweet_data is None:
        tweet_data = load_tweet_data()
    mention_data = tweet_data[tweet(tweet_data).str.contains(mention)]
    json_mention_data = mention_data.to_json(orient="records")
    return json.dumps({"data": json_mention_data})


@app.route("/get_available_attributes")
def get_available_attributes():
    global user_data
    if user_data is None:
        user_data = load_user_data(label="user")
        bot_data = load_user_data(filename="~/GIT/lda2vec/examples/twitter/data/content_polluters.txt", label="bot")
        user_data = pd.concat([user_data, bot_data])
        print(user_data.head(2))
    available_attributes = user_data.columns.values.tolist()
    return json.dumps({"attributes":available_attributes})


@app.route("/cluster_per_attributes")
def get_cluster_per_attributes():
    attributes = json.loads(request.args.get('data'))
    num_clusters = json.loads(request.args.get('num_clusters'))
    global user_data
    user_ids = user_data["user_id"].values.tolist()

    filtered_user_data = user_data[attributes]
    filtered_user_data = list(filtered_user_data.values)
    kmeans = KMeans(n_clusters=num_clusters)
    kmeans.fit(filtered_user_data)
    filtered_user_data = [list(item) for item in filtered_user_data]
    centroids = list(kmeans.cluster_centers_)
    centroids = [list(item) for item in centroids]
    labels = list(kmeans.labels_)
    labels = [int(item) for item in labels]
    from collections import Counter
    print(Counter(labels))

    if "label" in user_data.columns:
        label_identifiers = user_data["label"].values.tolist()
        return json.dumps({
            "data": filtered_user_data,
            "centroids": centroids,
            "labels": labels,
            "user_ids": user_ids,
            "dataset_labels" : label_identifiers
        })
    else:
        return json.dumps({
            "data" : filtered_user_data,
            "centroids" : centroids,
            "labels" : labels,
            "user_ids" : user_ids
        })


@app.route("/get_graph_structure")
def get_graph_structure():
    user_id = json.loads(request.args.get('user'))
    retweet_data, normal_tweets = load_retweet_data(str(user_id))
    graph = build_retweet_graph(retweet_data, user_id)
    graph = build_normal_tweet_graph(user_id, graph, normal_tweets)
    link_array = []
    for edge in graph.edges():
        from_node, to_node = edge
        link = {"source": from_node, "target": userid_to_screenname(to_node), "value": graph[from_node][to_node]["weight"],
                "indegree": 1, "linktype":graph[from_node][to_node]["linktype"], "msg":graph[from_node][to_node]["msg"]}
        link_array.append(link)
    return json.dumps({"links": link_array})


def build_normal_tweet_graph(user_id, graph, normal_tweets):
    for tweet_obj in normal_tweets:
        mentions = extract_mentions(tweet(tweet_obj).values[0])
        if len(mentions) == 0:
            date = tweet_obj.created_at.values[0]
            graph.add_edge(user_id,date )
            graph[user_id][date]["weight"] = 1
            graph[user_id][date]["msg"] = [tweet(tweet_obj).values[0]]
            graph[user_id][date]["linktype"] = "tweet"
        else:
            for mention in mentions:
                if not mention in graph.nodes():
                    graph.add_edge(user_id, mention)
                    graph[user_id][mention]["weight"] = 1
                    graph[user_id][mention]["msg"] = [tweet(tweet_obj).values[0]]
                else:
                    graph[user_id][mention]["weight"] += 1
                    msg_list = graph[user_id][mention]["msg"]
                    msg_list.append(tweet(tweet_obj).values[0])
                    graph[user_id][mention]["msg"] = msg_list
                graph[user_id][mention]["linktype"] = "mention"
    return graph

def build_retweet_graph(retweet_data, user_id):
    graph = networkx.DiGraph()
    users_in_edges = set([])
    for user, rt in retweet_data.items():
        insert_to_tweet_data(rt)
        tweet_list = rt
        for tweet in tweet_list:
            try:
                rt_status = tweet.retweeted_status
                retweet_msg = rt_status.text

                if not user in users_in_edges:
                    graph.add_edge(user_id, user)
                    graph[user_id][user]["weight"] = 1
                    graph[user_id][user]["msg"] = [retweet_msg]
                    users_in_edges.add(user)

                else:
                    graph[user_id][user]["weight"] += 1
                    msg_list = graph[user_id][user]["msg"]
                    msg_list.append(retweet_msg)
                    graph[user_id][user]["msg"] = msg_list
                graph[user_id][user]["linktype"]="retweet"
            except Exception as e:
                continue
    return graph


def insert_to_tweet_data(retweet_obj):
    global tweet_data, user_data
    tweet_list = retweet_obj
    for tweet in tweet_list:
        tweet_id = tweet.id_str
        tweet_obj = tweet_data[tweetid(tweet_data) == tweet_id]
        try:
            rt_status = tweet.retweeted_status
            is_retweet = True
            retweet_userid = rt_status.user.id_str
            retweet_msg = rt_status.text
            retweet_tweetid = rt_status.id
        except:
            is_retweet = False
            retweet_userid = None
            retweet_msg = None
            retweet_tweetid = None

        if len(tweet_obj) > 0:
            index = tweet_obj.index.values[0]
            tweet_data.at[index, "is_retweet"] = is_retweet
            tweet_data.at[index, "retweet_userid"] = retweet_userid
            tweet_data.at[index, "retweet_msg"] = retweet_msg
            tweet_data.at[index, "retweet_tweetid"] = retweet_tweetid
        else:
            tweet_data = tweet_data.append(
                {
                    "created_at":tweet.created_at,
                    "source":tweet.source,
                    "tweetid": tweet.id_str,
                    "text": tweet.text,
                    "userid": tweet.user.id_str,
                    "is_retweet": is_retweet,
                    "retweet_userid": retweet_userid,
                    "retweet_msg": retweet_msg,
                    "retweet_tweetid": retweet_tweetid
                }
            )
        user_obj = user_data[userid(user_data) == retweet_userid]
        if len(user_obj) == 0 and is_retweet:
            # insert this user into the user df
            user_info = rt_status.user
            user_data = user_data.append(
                {
                    "userid": user_info.id_str,
                    "created_at": user_info.created_at,
                    "description": user_info.description,
                    "follow_count": user_info.followers_count,
                    "friends_count": user_info.friends_count,
                    "location": user_info.location,
                    "name": user_info.name,
                    "profile_background_img": user_info.profile_background_image_url,
                    "profile_img": user_info.profile_image_url,
                    "screen_name": user_info.screen_name,
                    "url": user_info.url,
                    "statuses_count": user_info.statuses_count
                }, ignore_index=True
            )



def date_string_to_number(date_string):
    from datetime import datetime, timedelta
    date = datetime.strptime(date_string, "%Y-%m-%d %H:%M:%S")
    epoch = datetime(1970, 1, 1)

    td = date - epoch
    assert td.resolution == timedelta(microseconds=1)
    number = (td.days * 86400 + td.seconds) * 10 ** 6 + td.microseconds
    return number


def userid(df):
    try:
        return df.user_id
    except:
        return df.userid


def tweetid(df):
    try:
        return df.tweet_id
    except:
        return df.tweetid

def groupby_userid(df):
    try:
        return df.groupby(["user_id"])
    except:
        return df.groupby(["userid"])

def tweet(df):
    try:
        return df.tweet
    except:
        return df.text

def userid_to_screenname(user_id):
    global user_data
    try:
        screenname = user_data[userid(user_data) == user_id].screen_name.values[0]
        return screenname
    except:
        return user_id

def extract_mentions(t):
    import re
    mentions = re.findall(r"@(\w+)", t)
    return (mentions)

def softmax(x):
    # x has shape [batch_size, n_classes]
    e = np.exp(x)
    n = np.sum(e, 1, keepdims=True)
    return e/n


def separate_labels_from_data(data, label_col_name):
    labels = data[label_col_name]
    data.drop(label_col_name, axis=1, inplace=True)
    return data, labels


def split_train_test(data, labels, random_seed):
    data_train, data_test, label_train, label_test = train_test_split(data, labels, test_size=0.2, random_state=random_seed)
    return data_train, label_train, data_test, label_test


if __name__ == "__main__":
    try:
        app.run(host="10.147.68.237", port=5000)
    except:
        app.run(host="localhost", port=5002 )

