import json

import networkx as nx
import pandas as pd



class GraphBuilder():
    tweet_politics_df = None
    user_politics_df = None
    graph = None

    def init (self, tweet_fn, user_fn):
        """
        Load tweet and user data into dataframes. Initialize directed graph object.
        :param tweet_fn: filename of tweet data
        :param user_fn: filename of user data
        """

        self.tweet_politics_df = pd.read_pickle(tweet_fn)
        self.user_politics_df = pd.read_pickle(user_fn)
        self.graph = nx.DiGraph()

    def mentions(self):
        """
        Fill the graph with mention edges between a user and every user she mentions. Initial weight of such edge is set to 1,
        if a mention connection between two users occur more often, it is incremented for each occurrence.
        """

        for index, row in self.tweet_politics_df.iterrows():
            if not row.is_retweet:
                username = self.user_politics_df[userid(self.user_politics_df) == userid(row)].screen_name.values[0]
                if not username in self.graph.nodes():
                    self.graph.add_node(username)
                mentions = self._extract_mentions(tweet(row))
                for mention in mentions:
                    if not mention in self.graph.nodes():
                        self.graph.add_node(mention)
                    if not (username, mention) in self.graph.edges():
                        self.graph.add_edge(username, mention, type="mention", weight=1)
                    else:
                        self.graph[username][mention]["weight"] += 1



    def retweets(self):
        """
        Fill the graph with retweet edges between a user and every user she retweets. Initial weight of such edge is set to 1,
        if a retweet connection between two users occur more often, it is incremented for each occurrence.
        :return:
        """

        for index, row in self.tweet_politics_df.iterrows():
            if row.is_retweet:
                username = self.user_politics_df[userid(self.user_politics_df) == userid(row)].screen_name.values[0]
                if not username in self.graph.nodes():
                    self.graph.add_node(username)
                retweeted_user = row.retweet_user
                if not retweeted_user in self.graph.nodes():
                    self.graph.add_node(retweeted_user)
                if not (username, retweeted_user) in self.graph.edges():
                    self.graph.add_edge(username, retweeted_user, type="retweet", weight=1)
                else:
                    self.graph[username][retweeted_user]["weight"] += 1


    def _extract_mentions(self, tweet):
        """
        extract "@..." mentions from a tweet
        :param tweet: str
        :return: list of str
        """

        import re
        mentions = re.findall(r"@(\w+)", tweet)
        return mentions

    def _graph_to_json(self):
        nodes = list(self.graph.nodes())
        edges = list(self.graph.edges(data=True))

        return json.dumps({"nodes":nodes, "edges":edges})


def userid(df):
    try:
        return df.user_id
    except:
        return df.userid


def tweetid(df):
    try:
        return df.tweet_id
    except:
        return df.tweetid

def groupby_userid(df):
    try:
        return df.groupby(["user_id"])
    except:
        return df.groupby(["userid"])

def tweet(df):
    try:
        return df.tweet
    except:
        return df.text

"""
userdf = pd.DataFrame(columns=["userid","screen_name"])
tweetdf = pd.DataFrame(columns=["is_retweet","retweet_user","userid","text"] )
userdf = userdf.append({"userid": 1, "screen_name":"Test1"}, ignore_index=True)
userdf = userdf.append({"userid": 2, "screen_name":"Test2"}, ignore_index=True)
userdf = userdf.append({"userid": 3, "screen_name":"Test3"}, ignore_index=True)
tweetdf = tweetdf.append({"is_retweet" : False, "retweet_user": None, "userid":1, "text":"@Test2 check this new video out"}, ignore_index=True)
tweetdf = tweetdf.append({"is_retweet" : False, "retweet_user": None, "userid":2, "text":"@Test3 Have you seen this lately?"}, ignore_index=True)
tweetdf = tweetdf.append({"is_retweet" : True, "retweet_user": "Test1", "userid":3, "text":"RT @Test1 long time no see"}, ignore_index=True)
tweetdf = tweetdf.append({"is_retweet" : True, "retweet_user": "Test1", "userid":3, "text":"RT @Test1 long time no see again"}, ignore_index=True)

g = GraphBuilder()
g.graph = nx.DiGraph()
g.tweet_politics_df = tweetdf
g.user_politics_df = userdf
g.mentions()
g.retweets()

print(g._graph_to_json())
"""
