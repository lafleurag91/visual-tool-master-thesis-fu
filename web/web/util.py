def userid(df):
    try:
        return df.user_id
    except:
        return df.userid


def tweetid(df):
    try:
        return df.tweet_id
    except:
        return df.tweetid

def groupby_userid(df):
    try:
        return df.groupby(["user_id"])
    except:
        return df.groupby(["userid"])

def tweet(df):
    try:
        return df.tweet
    except:
        return df.text